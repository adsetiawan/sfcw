clear all;
addpath functions;
%% radar parameters
n_repeat=282;
samp_rate=44100;
n_steps=2*32;
%
count=n_steps*n_repeat*2;
filename='data/sfcw_data.dat';
s=read_complex_binary (filename, count);
t=1:length(s);
figure(1);
plot(t,real(s),'r',t,imag(s),'b');
idx_start=920;
% averaging over steps
for m=1:n_steps
    sIdx=idx_start+(m-1)*n_repeat;
    eIdx=idx_start+m*n_repeat;
    ts=s(sIdx:eIdx);
    tS(m,1)=mean(ts);
end;
n_fft=n_steps*8;
Y=zeros(n_fft,1);
Y(1:n_steps)=tS(1:n_steps,1);
Y(end:-1:end-n_steps+1)=conj(tS(1:n_steps));
  
y=fftshift(ifft(Y));
tx=1:length(y);
figure(22); 
subplot(121);plot(tx,real(y),'r',tx,imag(y),'b');
subplot(122);plot(abs(y));