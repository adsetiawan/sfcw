% ----------------------------------------------------
% FMCW SODAR SIMULATION
% Created by A.B. Suksmono/ STEI-ITB
% Dates: 4 May 2013
% ----------------------------------------------------
clear all;
%% natural constants
vs=343.2; % speed of sound

%% operating specs
samp_rate=44100; % sampling rate of soundcard
dt=1/samp_rate;
BW=1*10E3; % bandwidth 10 Khz
N_step=1*8;

%%
df = BW/N_step;
Ru=vs/(2*df);
min_tsens=2*Ru/vs;          % minimum sensing time
n_sens=round(min_tsens/dt); % n_repeat;
%% 

N_FFT=16*N_step;
N_FFT05=floor(N_FFT/2);
disp(sprintf('R_u: %3.4f, dR= %3.4f (m)',Ru, vs/BW)); 
disp(sprintf('t-dwell: %3.4f (s), N-dwell= %g (samples)',min_tsens, n_sens)); 

N_samp=N_step*n_sens;       % total complete samples
t=dt*(1:N_samp);
%
t_curr=1;
f0=0;
dN=n_sens;
% simulate object at 0.25 Ru
R_tgt=0.15*Ru;
tau=vs/(2*R_tgt);
for n=1:N_step
    f=f0+n*df; % current frequency
    t_curr=t(dN*(n-1)+1:n*dN);
    t_shift=t_curr-tau;
    s_tx((dN*(n-1)+1:n*dN))=cos(2*pi*f*t_curr);
    s_rx((dN*(n-1)+1:n*dN))=cos(2*pi*f*t_shift);
    % I/Q-demodulate received wave with transmitted wave
    re_IQ(n)=mean(cos(2*pi*f*t_curr).*cos(2*pi*f*t_shift)); %inphase
    im_IQ(n)=mean(sin(2*pi*f*t_curr).*cos(2*pi*f*t_shift)); %quadrature  
end;
half_RS(:,1)=re_IQ+1j*im_IQ;
RS=zeros(N_FFT,1);
RS(1:N_step,1)=half_RS(1:N_step);
RS(N_FFT:-1:N_FFT-N_step+1)= conj(half_RS(1:end));
rs=fftshift(ifft(RS));
figure(1);
subplot(121);plot(1:N_step,re_IQ,'r',1:N_step,im_IQ,'b');
subplot(122);plot(abs(rs));
% plot(rs);

    
