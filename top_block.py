#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Top Block
# Generated: Mon Apr 20 11:51:25 2015
##################################################

from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.gr import firdes
from gnuradio.wxgui import scopesink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx

class top_block(grc_wxgui.top_block_gui):

	def __init__(self):
		grc_wxgui.top_block_gui.__init__(self, title="Top Block")
		_icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
		self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

		##################################################
		# Variables
		##################################################
		self.samp_rate = samp_rate = 100000000

		##################################################
		# Blocks
		##################################################
		self.notebook_0 = self.notebook_0 = wx.Notebook(self.GetWin(), style=wx.NB_TOP)
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "tab1")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "tab2")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "tab3")
		self.Add(self.notebook_0)
		self.wxgui_scopesink2_0_0 = scopesink2.scope_sink_c(
			self.notebook_0.GetPage(1).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate/10,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(1).Add(self.wxgui_scopesink2_0_0.win)
		self.wxgui_scopesink2_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(0).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(0).Add(self.wxgui_scopesink2_0.win)
		self.low_pass_filter_0 = gr.fir_filter_fff(2, firdes.low_pass(
			1, samp_rate/10, samp_rate/100, samp_rate/1000, firdes.WIN_RECTANGULAR, 6.76))
		self.gr_vector_source_x_1 = gr.vector_source_f(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32, True, 1, [])
		    
		self.gr_vco_f_0 = gr.vco_f(samp_rate, 6280000, 32)
		self.gr_repeat_0 = gr.repeat(gr.sizeof_float*1, 1000)
		self.gr_multiply_xx_0 = gr.multiply_vff(1)
		self.gr_hilbert_fc_0 = gr.hilbert_fc(64)
		self.gr_delay_0 = gr.delay(gr.sizeof_float*1, 1000)

		##################################################
		# Connections
		##################################################
		self.connect((self.gr_vco_f_0, 0), (self.wxgui_scopesink2_0, 0))
		self.connect((self.gr_vector_source_x_1, 0), (self.gr_repeat_0, 0))
		self.connect((self.gr_repeat_0, 0), (self.gr_vco_f_0, 0))
		self.connect((self.gr_vco_f_0, 0), (self.gr_delay_0, 0))
		self.connect((self.gr_delay_0, 0), (self.gr_multiply_xx_0, 0))
		self.connect((self.gr_vco_f_0, 0), (self.gr_multiply_xx_0, 1))
		self.connect((self.gr_multiply_xx_0, 0), (self.low_pass_filter_0, 0))
		self.connect((self.low_pass_filter_0, 0), (self.gr_hilbert_fc_0, 0))
		self.connect((self.gr_hilbert_fc_0, 0), (self.wxgui_scopesink2_0_0, 0))


	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate
		self.wxgui_scopesink2_0.set_sample_rate(self.samp_rate)
		self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate/10, self.samp_rate/100, self.samp_rate/1000, firdes.WIN_RECTANGULAR, 6.76))
		self.wxgui_scopesink2_0_0.set_sample_rate(self.samp_rate/10)

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	(options, args) = parser.parse_args()
	tb = top_block()
	tb.Run(True)

