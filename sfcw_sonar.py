#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Sfcw Sonar
# Generated: Mon Apr 13 12:09:57 2015
##################################################

from gnuradio import audio
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import window
from gnuradio.eng_option import eng_option
from gnuradio.gr import firdes
from gnuradio.wxgui import fftsink2
from gnuradio.wxgui import scopesink2
from gnuradio.wxgui import waterfallsink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx

class sfcw_sonar(grc_wxgui.top_block_gui):

	def __init__(self):
		grc_wxgui.top_block_gui.__init__(self, title="Sfcw Sonar")
		_icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
		self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

		##################################################
		# Variables
		##################################################
		self.v_sound = v_sound = 343.2
		self.samp_rate = samp_rate = 44100
		self.n_steps = n_steps = 64
		self.n_repeat = n_repeat = 282
		self.bw = bw = 10*1000
		self.Gr = Gr = 1*1000

		##################################################
		# Blocks
		##################################################
		self.notebook_0 = self.notebook_0 = wx.Notebook(self.GetWin(), style=wx.NB_TOP)
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Tx")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "waveform")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Spectrum")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "demodulated")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "x")
		self.Add(self.notebook_0)
		self.wxgui_waterfallsink2_0 = waterfallsink2.waterfall_sink_c(
			self.notebook_0.GetPage(2).GetWin(),
			baseband_freq=0,
			dynamic_range=100,
			ref_level=0,
			ref_scale=2.0,
			sample_rate=samp_rate,
			fft_size=512,
			fft_rate=15,
			average=False,
			avg_alpha=None,
			title="Waterfall Plot",
		)
		self.notebook_0.GetPage(2).Add(self.wxgui_waterfallsink2_0.win)
		self.wxgui_scopesink2_1 = scopesink2.scope_sink_c(
			self.notebook_0.GetPage(0).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(0).Add(self.wxgui_scopesink2_1.win)
		self.wxgui_fftsink2_0 = fftsink2.fft_sink_c(
			self.notebook_0.GetPage(1).GetWin(),
			baseband_freq=0,
			y_per_div=10,
			y_divs=10,
			ref_level=0,
			ref_scale=2.0,
			sample_rate=samp_rate,
			fft_size=1024,
			fft_rate=15,
			average=False,
			avg_alpha=None,
			title="FFT Plot",
			peak_hold=False,
		)
		self.notebook_0.GetPage(1).Add(self.wxgui_fftsink2_0.win)
		self.low_pass_filter_0 = gr.fir_filter_ccf(1, firdes.low_pass(
			1, samp_rate, 1*bw, 0.01*bw, firdes.WIN_HAMMING, 6.76))
		self.gr_vector_source_x_0 = gr.vector_source_f(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64, True, 1, [])
		    
		self.gr_vco_f_0 = gr.vco_f(samp_rate, (2*3.14)*bw/n_steps, n_steps)
		self.gr_repeat_0 = gr.repeat(gr.sizeof_float*1, n_repeat)
		self.gr_multiply_xx_0_0 = gr.multiply_vff(1)
		self.gr_multiply_xx_0 = gr.multiply_vff(1)
		self.gr_multiply_const_vxx_0 = gr.multiply_const_vff((Gr, ))
		self.gr_hilbert_fc_0 = gr.hilbert_fc(64)
		self.gr_float_to_complex_0 = gr.float_to_complex(1)
		self.gr_file_sink_0 = gr.file_sink(gr.sizeof_gr_complex*1, "/home/suksmono/Desktop/SDRprojects/3. SFCW/MATSRC/data/sfcw_data.dat")
		self.gr_file_sink_0.set_unbuffered(False)
		self.gr_complex_to_float_0 = gr.complex_to_float(1)
		self.audio_source_0 = audio.source(samp_rate, "", True)
		self.audio_sink_0 = audio.sink(samp_rate, "", True)

		##################################################
		# Connections
		##################################################
		self.connect((self.audio_source_0, 0), (self.gr_multiply_const_vxx_0, 0))
		self.connect((self.gr_vector_source_x_0, 0), (self.gr_repeat_0, 0))
		self.connect((self.gr_repeat_0, 0), (self.gr_vco_f_0, 0))
		self.connect((self.gr_vco_f_0, 0), (self.gr_hilbert_fc_0, 0))
		self.connect((self.gr_hilbert_fc_0, 0), (self.gr_complex_to_float_0, 0))
		self.connect((self.gr_complex_to_float_0, 0), (self.audio_sink_0, 0))
		self.connect((self.gr_complex_to_float_0, 0), (self.gr_multiply_xx_0, 0))
		self.connect((self.gr_multiply_const_vxx_0, 0), (self.gr_multiply_xx_0, 1))
		self.connect((self.gr_multiply_const_vxx_0, 0), (self.gr_multiply_xx_0_0, 1))
		self.connect((self.gr_complex_to_float_0, 1), (self.gr_multiply_xx_0_0, 0))
		self.connect((self.gr_multiply_xx_0, 0), (self.gr_float_to_complex_0, 0))
		self.connect((self.gr_multiply_xx_0_0, 0), (self.gr_float_to_complex_0, 1))
		self.connect((self.gr_float_to_complex_0, 0), (self.low_pass_filter_0, 0))
		self.connect((self.low_pass_filter_0, 0), (self.gr_file_sink_0, 0))
		self.connect((self.low_pass_filter_0, 0), (self.wxgui_fftsink2_0, 0))
		self.connect((self.low_pass_filter_0, 0), (self.wxgui_scopesink2_1, 0))
		self.connect((self.low_pass_filter_0, 0), (self.wxgui_waterfallsink2_0, 0))


	def get_v_sound(self):
		return self.v_sound

	def set_v_sound(self, v_sound):
		self.v_sound = v_sound

	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate
		self.wxgui_fftsink2_0.set_sample_rate(self.samp_rate)
		self.wxgui_scopesink2_1.set_sample_rate(self.samp_rate)
		self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, 1*self.bw, 0.01*self.bw, firdes.WIN_HAMMING, 6.76))
		self.wxgui_waterfallsink2_0.set_sample_rate(self.samp_rate)

	def get_n_steps(self):
		return self.n_steps

	def set_n_steps(self, n_steps):
		self.n_steps = n_steps

	def get_n_repeat(self):
		return self.n_repeat

	def set_n_repeat(self, n_repeat):
		self.n_repeat = n_repeat

	def get_bw(self):
		return self.bw

	def set_bw(self, bw):
		self.bw = bw
		self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, 1*self.bw, 0.01*self.bw, firdes.WIN_HAMMING, 6.76))

	def get_Gr(self):
		return self.Gr

	def set_Gr(self, Gr):
		self.Gr = Gr
		self.gr_multiply_const_vxx_0.set_k((self.Gr, ))

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	(options, args) = parser.parse_args()
	tb = sfcw_sonar()
	tb.Run(True)

